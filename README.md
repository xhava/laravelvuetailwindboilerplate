<p align="center">
<img src="http://wootbit.io/images/logos/LOGOwootbit60x61.png" width="60">
</p>

#Wootbit

## About Laravel7 + Vue Js + Tailwind Boilerplate
It serves as a starting point for the start of a project, it includes the sig. packages as well
, includes login and register form.


- Laravel 7
- Vue js 2
- Vue router
- Tailwind 1.2
- Laravel Passport por api authentication


##What you need
* PHP >= 7.2.5
* BCMath PHP Extension
* Ctype PHP Extension
* Fileinfo PHP extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension


##How Install

- clone the repo in your computer.
- run command: *composer install* , in the root of your project.
- create your database.
- rename the file *.env.example* to *.env*.
- configure your database credentials in the *.env* file.
- run command: *php artisan key:generate*.
- run the command: *php artisan migrate*, for generate migrations.
- run command: *php artisan passport:install*, for create personal and password grant secret keys



## License

This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
