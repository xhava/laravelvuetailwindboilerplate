<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-gray-300">
<div id="app">
    <nav class="flex items-center justify-between flex-wrap bg-indigo-900 p-6">

        <div class="flex items-center flex-shrink-0 text-black-50 mr-6">
            <img src="{{ asset('images/LOGOwootbit60x61.png') }}" alt="">
            <span class="font-semibold text-xl tracking-tight">
                Wootbit
            </span>
        </div>


        <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
            <div>
                <a href="{{ url('/') }}"
                   class="shadow-inner inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo-800 hover:bg-white mt-4 lg:mt-0">
                    Home Page
                </a>
            </div>
            <div class="text-sm lg:flex-grow">
                <div class="bg-indigo-900 text-center py-4 lg:px-4">
                    <div
                        class="p-2 bg-indigo-800 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex"
                        role="alert">
                        <span class="flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">
                            Create Some New Cool Project
                        </span>
                        <span class="font-semibold mr-2 text-left flex-auto">
                            Laravel 7 + Vue 2.2 + Tailwind Boilerplate Configuration
                        </span>
                    </div>
                </div>
            </div>
            <div>
                @guest
                    <a href="{{ route('login') }}"
                       class="shadow-inner inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo-800 hover:bg-white mt-4 lg:mt-0">
                        Login
                    </a>

                    <a href="{{ route('register') }}"
                       class="inline-block shadow-inner text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo-800 hover:bg-white mt-4 lg:mt-0">
                        Register
                    </a>
                @else
                    <a href="#"
                       class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo-800 hover:bg-white mt-4 lg:mt-0">
                        {{ 'What\'s up: ' . auth()->user()->name }}
                    </a>
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                       class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo-800 hover:bg-white mt-4 lg:mt-0">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endguest


            </div>
        </div>
    </nav>

    <main>
        @yield('content')
    </main>
</div>
</body>
</html>
