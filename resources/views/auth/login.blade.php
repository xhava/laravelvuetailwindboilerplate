@extends('layouts.app')

@section('content')
    <div class="text-center py-4 lg:px-4">
        <div class="p-20 items-center text-indigo-100 leading-none flex lg:inline-flex">
            <form class="w-full max-w-sm" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="md:flex md:items-center mb-6">
                    <div class="md:w-1/3">

                        <label class="block text-gray-600 font-bold md:text-right mb-1 md:mb-0 pr-4"
                               for="inline-full-name">
                            {{ __('E-mail') }}
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <input
                            class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                            id="inline-full-name" type="text" placeholder="John Doe"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <p class="text-red-400 pt-1 text-sm">{{ $message }}</p>
                        @enderror
                    </div>


                </div>
                <div class="md:flex md:items-center mb-6">
                    <div class="md:w-1/3">
                        <label class="block text-gray-600 font-bold md:text-right mb-1 md:mb-0 pr-4"
                               for="inline-username">
                            {{ __('Password') }}
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <input
                            class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                            id="inline-username" name="password" type="password" placeholder="******************" required autocomplete="current-password">
                        @error('password')
                        <p class="text-red-400 pt-1 text-sm">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="md:flex md:items-center">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3">
                        <button
                            class="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                            type="submit">
                            Login
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="flex items-center">
        <div class="p-10">

        </div>
    </div>
@endsection
