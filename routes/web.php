<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('{any}', 'AppController')
    ->where('any', '.*')
    ->middleware('auth')
    ->name('home');
